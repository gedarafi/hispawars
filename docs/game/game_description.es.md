# Descripción del juego

Hispawars es un juego de estrategia multijugador ambientado en una parcela de tierra con agua, obstáculos y comida que cae al azar. Cada jugador tiene uno o más hormigueros donde aparecerán las hormigas. El objetivo es que los jugadores busquen y destruyan las colonias de hormigas enemigas mientras defienden a la propia. Los jugadores también deben recolectar comida para generar más hormigas. Si se destruyen todos los hormigueros de un jugador, no puede generar más hormigas.

El desafío es crear un programa de computadora (un bot) que juegue el juego de la manera más inteligente posible. Se recomienda que utilice uno de los paquetes de inicio como punto de partida.

## El Campo de batalla
El mapa es una cuadrícula que se une nuevamente por los bordes. Esto significa que si una hormiga camina por la parte superior del mapa, aparecerá en la parte inferior, o si camina hacia la derecha, aparecerá a la izquierda. Cada bot lee información sobre los cuadrados que puede ver actualmente y emite órdenes para mover sus hormigas por el mapa. El juego se basa en turnos.

Cada hormiga solo puede ver el área a su alrededor, por lo que los bots no comenzarán con una vista completa del mapa. Cada turno, el bot recibirá la siguiente información para todos los cuadrados que sean visibles para sus hormigas:

- una lista de cuadrados de agua, que no se han visto antes
- una lista de hormigas, incluido el propietario
- una lista de comida
- una lista de hormigueros, incluido el propietario
- una lista de hormigas muertas (de la última fase de ataque), incluido el propietario

Un bot puede emitir hasta una orden por cada hormiga durante un turno. Cada orden especifica una hormiga por ubicación y la dirección para moverla: norte, sur, este u oeste. Una vez que se ejecuta la orden, las hormigas se mueven un cuadrado en la dirección indicada.

## Fases del juego
Luego, el juego pasa por 5 fases:

- se mueven todas las hormigas (las hormigas que chocan en el mismo cuadrado mueren)
- se ataca a las hormigas enemigas si están dentro del alcance
- se destruyen hormigueros con hormigas enemigas colocadas sobre ellos
- se generan más hormigas en las colonias que no están arrasadas ni bloqueadas
- se recoge la comida cercana a las hormigas (la comida desaparece si hay 2 enemigos al lado)

Después de las fases, el bot recibirá el siguiente estado del juego y emitirá más movimientos.

A veces, los bots fallan o colapsan (¡no dejes que tu bot haga esto!) y se eliminan del juego. Las hormigas simplemente se quedarán donde están y aún deben ser atacadas y asesinadas para obtener su territorio. 

Es bueno controlar la mayor parte del mapa, porque entonces el bot podrá recolectar más comida para crear más hormigas, lo que le dará una mejor oportunidad de arrasar los hormigueros enemigos mientras defiende los suyos, ¡que es la forma de obtener la puntuación más alta y ganar!

## Final del juego
El juego termina cuando solo un jugador activo tiene hormigas, o solo quedan los hormigueros de un jugador, o si el juego pasa de un cierto número de turnos. El juego también se puede interrumpir si ningún bot está progresando, o si la puntuación de los bots no cambia.