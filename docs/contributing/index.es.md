# Guía para contribuir en el proyecto
¡Muchas gracias por tu interés en participar!

Todo el contenido es mantenido por voluntarios que aportan su tiempo y esfuerzo. Antes de empezar: te pedimos ser **amable y respetuoso** a la hora de participar.

Actualmente estamos buscando:

- **Mejorar el código** del motor del juego (hacerlo compatible con Python 3 y correcciones de estilo)

- Crear una **plataforma (posiblemente en Django)** para organizar a los participantes y mostrar el desarrollo de la competición

Sientete libre de tomar algún issue pendiente o directamente puedes escribir en nuestro [grupo en Telegram](https://t.me/hispawars).