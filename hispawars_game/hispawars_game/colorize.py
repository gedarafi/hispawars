import sys
import colorama


colorama.init()


class Colorize(object):
    def __init__(self, file, color=colorama.Fore.RED):
        self.file = file
        self.color = color
        self.reset = colorama.Style.RESET_ALL

    def write(self, data):
        if self.color:
            self.file.write("".join(self.color))
        self.file.write(data)
        if self.reset:
            self.file.write("".join(self.reset))

    def flush(self):
        self.file.flush()

    def close(self):
        self.file.close()


stderr = Colorize(sys.stderr)
