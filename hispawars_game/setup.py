import setuptools


setuptools.setup(name='hispawars_game',
                 version='0.1.0',
                 packages=setuptools.find_packages(),
                 install_requires=['colorama'])
